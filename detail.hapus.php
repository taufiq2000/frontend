<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- Judul -->
<div class="card text-center">
  <div class="card-body">
    <h1 class="card-title">Detail Tanggal</h1>
  </div>
</div>

<!-- Tabel Kegiatan -->
<div class="container">
<div class="row">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col"></th>
      <th scope="col">Nama Kegiatan</th>
      <th scope="col">Tanggal</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <th scope="row"></th>
      <td> 
          <div class="hapuse">
                
          </div>
      </td>
      <td> </td>
    </tr>
  </tbody>

</table>
</div>
<div class="card-body">
    <h1 class="card-title"></h1>
</div>

<!-- Tombol untuk tambah, edit, hapus, dan kembali -->
<a class="btn btn-primary btn-md" href="detail.tambah.php">Tambah Agenda</a>
<a class="btn btn-secondary btn-md" href="detail.php" data-toggle="modal">Edit Agenda</a>
<a class="btn btn-danger btn-md" href="detail.php">Hapus Agenda</a>
<a class="btn btn-light btn-md" href="daftar.php">Kembali</a>
<div class="card-body">
    <h3 class="card-title"></h3>
</div>

    <!-- Untuk Hapus agenda -->
    <form>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Yakin untuk menghapus ?</label>
          <button type="submit" id="infoDeleteData" class="btn btn-danger" href="detail.php">Hapus</button>
        </div>
    </form>
    <script src="script.js"></script>
    <script>
        const form = document.querySelector('form');
        const ul = document.querySelector('.hapuse');
        const button = document.querySelector('button');
        const input = document.getElementById('inputData');

        const buatLi = (text) => {
        const li = document.createElement('div');
        li.textContent = text;
        ul.appendChild(li);
        };

        // Untuk menampilkan input yang telah disimpan
        let itemArray = localStorage.getItem('item') ? JSON.parse(localStorage.getItem('item')) : [];

        localStorage.setItem('item', JSON.stringify(itemArray));
        const data = JSON.parse(localStorage.getItem('item'));

        // Perulangan
        data.forEach((item) => {
        buatLi(item);
        });

        // Untuk hapus
        button.addEventListener('click', function () {
            localStorage.removeItem('item')
        });
    </script>
</div>
</div>


