<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- Bar -->
<div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link" aria-current="true" href="index.php">Halaman Utama</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" aria-current="true" href="#">Tabel Agenda</a>
      </li>
    </ul>
  </div>

   <!-- Judul -->
  <div class="card-body">
    <h1 class="card-title">To-Do-List</h5>

    <!-- Dropdown untuk sortir bulan dan tahun -->
    <div class="btn-group">
  <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Bulan
  </button>
  <ul class="dropdown-menu">
    <li><a class="dropdown-item" href="#">Januari</a></li>
    <li><a class="dropdown-item" href="#">Februari</a></li>
    <li><a class="dropdown-item" href="#">Maret</a></li>
    <li><a class="dropdown-item" href="#">April</a></li>
    <li><a class="dropdown-item" href="#">Mei</a></li>
    <li><a class="dropdown-item" href="#">Juni</a></li>
    <li><a class="dropdown-item" href="#">Juli</a></li>
    <li><a class="dropdown-item" href="#">Agustus</a></li>
    <li><a class="dropdown-item" href="#">September</a></li>
    <li><a class="dropdown-item" href="#">Oktober</a></li>
    <li><a class="dropdown-item" href="#">November</a></li>
    <li><a class="dropdown-item" href="#">Desember</a></li>
  </ul>
</div>
<div class="btn-group">
<button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
    Tahun
  </button>
  <ul class="dropdown-menu">
    <li><a class="dropdown-item" href="#">2020</a></li>
    <li><a class="dropdown-item" href="#">2021</a></li>
    <li><a class="dropdown-item" href="#">2022</a></li>
  </ul>
</div>
<button class="btn btn-outline-secondary" type="submit">Search</button>
  </div>
</div>

<!-- Tabel Kegiatan -->
<div class="container">
<div class="row">
<table class="table">
    <thead class="thead-dark">
        <tr>
          <th scope="col"></th>
          <th scope="col">Nama Kegiatan</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row"></th>
          <td>Meeting dengan Client</td>
          <!-- Untuk melihat detail tanggal -->
          <td><a class="btn btn-success btn-sm" href="detail.php">Detail Tanggal</a></td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>Survey Lokasi</td>
           <!-- Untuk melihat detail tanggal -->
          <td><a class="btn btn-success btn-sm" href="detail.php">Detail Tanggal</a></td>
        </tr>
        <tr>
          <th scope="row"></th>
          <td>Pemilihan alat untuk proyek</td>
           <!-- Untuk melihat detail tanggal -->
          <td><a class="btn btn-success btn-sm" href="detail.php">Detail Tanggal</a></td>
        </tr>
      </tbody>
</table>
</div>
</div>
</div>


