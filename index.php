<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<!-- Bar -->
<div class="card text-center">
  <div class="card-header">
    <ul class="nav nav-tabs card-header-tabs">
      <li class="nav-item">
        <a class="nav-link active" aria-current="true" href="#">Halaman Utama</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" aria-current="true" href="daftar.php">Tabel Agenda</a>
      </li>
    </ul>
  </div>

  <!-- Judul -->
  <div class="card-body">
    <h1 class="card-title">To-Do-List</h1>
    <p class="card-text">Daftar Agenda Hari Ini</p>
  </div>
</div>

<!-- Tabel Kegiatan -->
<div class="container">
<div class="row">
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col"></th>
      <th scope="col">Nama Kegiatan</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"></th>
      <td>Meeting dengan Client</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td>Survey Lokasi</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td>Pemilihan alat untuk proyek</td>
    </tr>
  </tbody>
</table>
</div>
</div>
</div>


